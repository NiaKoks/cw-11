const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/',async (req,res)=>{
    const user = new User(req.body);
    user.generateToken();

    try{
        await user.save();
        return res.send({message: "User registred", user});
    } catch (e) {
        return res.status(400).send(e);
    }
});
router.post('/sessions',async (req,res)=>{
    const user = await User.findOne({username: req.body.username});

    if (!user){
        return res.status(400).send({error: 'User doesn\'t exist'})
    }
    user.generateToken();
    await user.save();

    res.send({message: 'OK', user});
});

module.exports = router;