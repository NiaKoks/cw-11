const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth')

const Product = require('../models/Product');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Product.find().populate('category', '_id title')
        .then(products => res.send(products))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
    Product.findById(req.params.id)
        .then(product => {
            if (product) res.send(product);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', [upload.single('img'), auth], (req, res) => {
    const productData = req.body;

    if (req.file) {
        productData.img = req.file.filename;
    }
    productData.seller = req.user._id;
    const product = new Product(productData);

    product.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});


module.exports = router;