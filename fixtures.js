const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Product = require('./models/Product');
const Category = require('./models/Category');


const run = async  () =>{
    await mongoose.connect(config.dbUrl,config.mongoOptions);

    const connection = mongoose.connection;
    const collections = await connection.db.collections();

    for (let collection of collections){ await collection.drop()}

    const [user, user2,user3] = await User.create(
        {
            username: "Freddy",
            password: '123',
            displayname: "Her Majesty",
            phonenumber: "0500123456",
            token: '007865'
        },
        {
            username: "Mandy",
            password: '234',
            displayname: "StyleIn",
            phonenumber: "0500345678",
            token: '123490'
        },
        {
            username: "Bobby",
            password: '098',
            displayname: "4U",
            phonenumber: "0500772600",
            token: '234567'
        }
    );

    const [category, category2,category3] = await Category.create(
        {
            title: 'First Category'
        },
        {
            title: 'Second Category'
        },
        {
            title: 'Third Category'
        }
    );

    const [product, product2,product3] = await Product.create(
        {
            title:'Shiver',
            description: "Amazing product",
            img: 'anon.png',
            category: category2._id,
            seller: user3._id,
            price: '123'
        },
        {
            title:'Mimosa',
            description: "Amazing product,trust me",
            img: 'mimosa.jpeg',
            category: category3._id,
            seller: user2._id,
            price: '1830'
        },
        {
            title:'Dream',
            description: "Amazing one",
            img: 'dream.jpg',
            category: category._id,
            seller: user._id,
            price: '1230'
        }
    );
    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});
